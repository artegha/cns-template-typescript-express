# cns-template-typescript

This is the official TypeScript Express template for [Create Node Server](https://github.com/artegha/create-node-server).

To use this template, add `--template typescript-express` when creating a new server.

For example:

```sh
npx @artegha/create-node-server my-server --template typescript-express

# or

yarn create @artegha/node-server my-server --template typescript-express
```

For more information, please refer to:

- [Hello World](https://expressjs.com/en/starter/hello-world.html) – How to develop APIs bootstrapped with Express.