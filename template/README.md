This project was bootstrapped with [Create Node Server](https://github.com/artegha/create-node-server).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the server in the development mode.<br />
It will be listening on [http://localhost:3000](http://localhost:3000) by default.

The server will restart if you make edits.<br />
You will also see any lint errors in the console.

### `npm run build`

Builds the server for production to the `dist` folder.<br />
It correctly bundles in production mode and optimizes the build for the best performance.

The build is minified.<br />
Your server is ready to be deployed!

### `npm run prod`

Builds and Run the server for production.<br />

### `npm run linter`

Use [ESLint](https://eslint.org/docs/user-guide/getting-started) to highlight errors and format your code following `.eslintrc` configuration file.
[ESLint](https://eslint.org/docs/user-guide/getting-started) is a tool for identifying and reporting on patterns found in ECMAScript/JavaScript code. 

### `npm run prettier`

Use [Prettier](https://prettier.io/docs/en/cli.html) to format your code, you can add following `.prettierrc` configuration file to customize your rules.
[Prettier](https://prettier.io/docs/en/cli.html) is an opinionated code formatter. It enforces a consistent style by parsing your code and re-printing it with its own rules that take the maximum line length into account, wrapping code when necessary.
