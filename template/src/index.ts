import dotenv from 'dotenv'
dotenv.config()
import express from 'express'
import cors from 'cors'
import helmet from 'helmet'
import compression from 'compression'
import morgan from 'morgan'

const server = express()

server.use(morgan('dev'))
server.disable('x-powered-by')
server.use(cors())
server.use(compression())
server.use(helmet())

server.listen(3000, async () => {
	try {
		console.log('Listening on port 3000 !')
	} catch (error) {
		console.log(error)
	}
})
